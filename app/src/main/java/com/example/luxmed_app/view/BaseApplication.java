package com.example.luxmed_app.view;

import android.app.Application;

import com.example.luxmed_app.di.components.AppComponent;
import com.example.luxmed_app.di.components.AppComponentDetails;
import com.example.luxmed_app.di.components.DaggerAppComponent;
import com.example.luxmed_app.di.components.DaggerAppComponentDetails;


public class BaseApplication extends Application {

    private AppComponent appComponent;
    private AppComponentDetails appComponentDetails;


    @Override
    public void onCreate() {
        super.onCreate();
        appComponent= DaggerAppComponent.create();
        appComponentDetails = DaggerAppComponentDetails.create();
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }

    public AppComponentDetails getAppComponentDetails() {
        return appComponentDetails;
    }
}
