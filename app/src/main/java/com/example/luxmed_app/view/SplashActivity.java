package com.example.luxmed_app.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.example.luxmed_app.R;
import com.example.luxmed_app.databinding.SplashBinding;
import com.example.luxmed_app.viewmodel.SplashViewModel;

public class SplashActivity extends AppCompatActivity {
    private SplashBinding binding;
    private SplashViewModel viewModel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_splash);
        viewModel = ViewModelProviders.of(this).get(SplashViewModel.class);
        binding.setSplash(viewModel);
        viewModel.getElapsedTime().observe(this, time ->{
            if(!viewModel.isComplete()){
                Log.d("newTime!",time.toString());
                viewModel.setVisible(false);
                viewModel.setComplete(true);
                Intent intent = new Intent(SplashActivity.this, BooksActivity.class);
                startActivity(intent);
            }
        });
    }
}
