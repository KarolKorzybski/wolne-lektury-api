package com.example.luxmed_app.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;
import android.util.Log;

import com.example.luxmed_app.R;
import com.example.luxmed_app.databinding.BookDetailsBinding;
import com.example.luxmed_app.viewmodel.BookDetailsViewModel;

import javax.inject.Inject;

public class BookDetailsActivity extends AppCompatActivity {
    private BookDetailsBinding binding;
    private BookDetailsViewModel viewModel;
    private String href = "";

    @Inject
    ViewModelProvider.Factory viewModelFactory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle extras = getIntent().getExtras();
        if (extras == null) {
            Log.d("isEmpty", "empty");
        } else {
            href = extras.getString("href");
        }
        binding = DataBindingUtil.setContentView(this, R.layout.activity_book_details);
        ((BaseApplication) getApplication()).getAppComponentDetails().inject(this);
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(BookDetailsViewModel.class);
        viewModel.setHref(href);
//        binding.setViewModel(viewModel);
        viewModel.getModelMutableLiveData().observe(this, bookDetails -> {
            binding.setBooksDetails(bookDetails);
        });
    }
}
