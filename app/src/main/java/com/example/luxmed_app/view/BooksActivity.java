package com.example.luxmed_app.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.example.luxmed_app.R;
import com.example.luxmed_app.adapter.BookAdapter;
import com.example.luxmed_app.databinding.ActivityBooksBinding;
import com.example.luxmed_app.model.Book;
import com.example.luxmed_app.viewmodel.BooksViewModel;

import java.util.ArrayList;

import javax.inject.Inject;

public class BooksActivity extends AppCompatActivity {
    private ActivityBooksBinding binding;
    private BooksViewModel viewModel;
    private BookAdapter adapter;
    private boolean busy = false;
    private int counter = 0;
    private static final String TAG = "BooksActivity";

    @Inject
    ViewModelProvider.Factory viewModelFactory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        busy = true;
        binding = DataBindingUtil.setContentView(this, R.layout.activity_books);
        ((BaseApplication) getApplication()).getAppComponent().inject(this);
        binding.setLifecycleOwner(this);
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(BooksViewModel.class);
        binding.setViewModel(viewModel);

        viewModel.getModelMutableLiveData().observe(this, this::setBinding);
        viewModel.getModelMutableUrlDetails().observe(this, book ->{
            if(book.isBusy()) return;
            book.setBusy(true);
            viewModel.setBook(book);
            adapter.notifyDataSetChanged();
            if(book.getHref().equals("")) return;
            intentToDetailsBook(modifyText(book.getHref()));
            busy = false;
        });
    }
    private void setBinding(ArrayList<Book> books){
        adapter = new BookAdapter(books, viewModel);
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(BooksActivity.this));
        binding.recyclerView.setAdapter(adapter);
        viewModel.getComplete().set(true);
        viewModel.getVisible().set(true);
        adapter.notifyDataSetChanged();
        init();
    }


    private String modifyText(String text){

        String getString = getResources().getString(R.string.url_network_module);
        String urlModify = text.replace(getString, "");
        urlModify = urlModify.replace("/", "");
        return urlModify;
    }

    @Override
    protected void onResume() {
        super.onResume();
        counter = 0;
        if(viewModel.getBook()!=null && adapter!=null && viewModel!=null) {
            init();
        }
        if(!busy && viewModel!=null){
            viewModel.getComplete().set(true);

        }
    }

    private void init(){
        if(viewModel.getBook()!=null) viewModel.getBook().setClick(false);
        adapter.notifyDataSetChanged();
        viewModel.setClick(false);

    }

    private void intentToDetailsBook(String href){

        Intent intent = new Intent(BooksActivity.this, BookDetailsActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("href", href);
        startActivity(intent);

    }
    @Override
    public void onBackPressed() {
        counter++;
        if(counter==1){
            Toast.makeText(getBaseContext(),"Are you sure want to exit?",
                    Toast.LENGTH_SHORT).show();
        }else{
            finishAffinity();
            System.exit(0);
        }
    }
}
