package com.example.luxmed_app.rest;

import com.example.luxmed_app.model.Book;
import com.example.luxmed_app.model.BookDetails;

import java.util.ArrayList;

import io.reactivex.Observable;
import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Path;

public interface BookService {
    @GET("/api/authors/?format=json")
    Single<ArrayList<Book>> getBooks();
    @Headers({
            "Content-Type: application/json"
    })
    @GET("/api/authors/{url}/")
    Single<BookDetails> getDetailsBooks(
            @Path("url") String url
    );
}
