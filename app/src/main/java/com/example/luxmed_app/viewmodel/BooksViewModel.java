package com.example.luxmed_app.viewmodel;

import android.util.Log;

import androidx.databinding.ObservableBoolean;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.luxmed_app.model.Book;
import com.example.luxmed_app.repository.UserRepository;

import java.util.ArrayList;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class BooksViewModel extends ViewModel {


    private UserRepository userRepository;
    private final CompositeDisposable disposable = new CompositeDisposable();
    private final MutableLiveData<ArrayList<Book>> modelMutableLiveData = new MutableLiveData<>();
    private final MutableLiveData<Book> modelMutableUrlDetails = new MutableLiveData<>();
    private ArrayList<Book> booksArray;
    private Book book;

    private String url;
    private String href;
    private String name;
    private String slug;
    ObservableBoolean complete = new ObservableBoolean();
    ObservableBoolean visible = new ObservableBoolean();
    private boolean click = false;

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public ObservableBoolean getVisible() {
        return visible;
    }

    public void setVisible(ObservableBoolean visible) {
        this.visible = visible;
    }

    public ObservableBoolean getComplete() {
        return complete;
    }

    public void setComplete(ObservableBoolean complete) {
        this.complete = complete;
    }

    public boolean isClick() {
        return click;
    }

    public void setClick(boolean click) {
        this.click = click;
    }

    public MutableLiveData<Book> getModelMutableUrlDetails() {
        return modelMutableUrlDetails;
    }

    public void setModelMutableUrlDetails(Book book){
        modelMutableUrlDetails.setValue(book);
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public BooksViewModel(String url, String href, String name, String slug) {
        this.url = url;
        this.href = href;
        this.name = name;
        this.slug = slug;
    }

    @Inject
    public BooksViewModel(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public BooksViewModel() {
    }

    private void loadData() {
        disposable.add(userRepository.modelSingle()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<ArrayList<Book>>() {
                    @Override
                    public void onSuccess(ArrayList<Book> books) {
                        booksArray = new ArrayList<>();
                        booksArray.clear();
                        booksArray.addAll(books);
                        getModelMutableLiveData().setValue(booksArray);
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d("error",e.toString());
                        loadData();
                    }
                }));
    }

    public MutableLiveData<ArrayList<Book>> getModelMutableLiveData() {
        if(booksArray==null || booksArray.isEmpty()) loadData();
        return modelMutableLiveData;
    }
    @Override
    protected void onCleared() {
        super.onCleared();
        if (disposable != null) {
            disposable.clear();

        }
    }
}
