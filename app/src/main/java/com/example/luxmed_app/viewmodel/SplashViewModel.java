package com.example.luxmed_app.viewmodel;

import android.os.SystemClock;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.Timer;
import java.util.TimerTask;

public class SplashViewModel extends ViewModel {
    private static final int THREE_SECOND = 3000;
    private Boolean visible = true;
    public Boolean getVisible() {
        return visible;
    }

    public void setVisible(Boolean visible) {
        this.visible = visible;
    }

    private MutableLiveData<Long> mElapsedTime = new MutableLiveData<>();

    private long mInitialTime = 0L;
    private boolean complete = false;

    public boolean isComplete() {
        return complete;
    }

    public void setComplete(boolean complete) {
        this.complete = complete;
    }

    public SplashViewModel() {
        if(mInitialTime!=0L) return;
        mInitialTime = SystemClock.elapsedRealtime();
        Timer timer = new Timer();

        // Update the elapsed time every second.
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                final long newValue = (SystemClock.elapsedRealtime() - mInitialTime) / 1000;
                mElapsedTime.postValue(newValue);
                stopTimer(timer);
            }
        }, THREE_SECOND, THREE_SECOND);

    }
    private void stopTimer(Timer timer){
        if(timer != null) {
            timer.cancel();
            timer.purge();
            timer = null;
        }
    }

    public LiveData<Long> getElapsedTime() {
        return mElapsedTime;
    }
}
