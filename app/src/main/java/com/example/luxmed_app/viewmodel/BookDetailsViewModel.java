package com.example.luxmed_app.viewmodel;

import android.graphics.Color;
import android.webkit.WebView;

import androidx.databinding.BindingAdapter;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.luxmed_app.model.BookDetails;
import com.example.luxmed_app.repository.UserRepository;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class BookDetailsViewModel extends ViewModel {
    private final static String ASSETS_PREFIX = "file:///android_asset/";
    private final static String HTML_HEADER = "<head>" +
            "<meta charset=\"UTF-8\">" +
            "<link rel=\"stylesheet\" type=\"text/css\" href=\"" + ASSETS_PREFIX + "style_event.css\">" +
            "</head>";

    private UserRepository userRepository;
    private final CompositeDisposable disposable = new CompositeDisposable();
    private final MutableLiveData<BookDetails> modelMutableLiveData = new MutableLiveData<>();
    public BookDetails mBookDetails;

    private String name;
    private String url;
    private String sortKey;
    private String description;
    private String href;

    public BookDetailsViewModel(BookDetails bookDetails) {
        this.name = bookDetails.getName();
        this.description = bookDetails.getDescription();
        this.sortKey = bookDetails.getSortKey();
        this.url = bookDetails.getUrl();
    }

    public BookDetailsViewModel() {
    }

    @Inject
    public BookDetailsViewModel(UserRepository userRepository) {
        this.userRepository = userRepository;
    }
    private void loadData() {
        disposable.add(userRepository.modelDetailsSingle(href)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<BookDetails>() {
                    @Override
                    public void onSuccess(BookDetails bookDetails) {
                        mBookDetails = new BookDetails(bookDetails);
                        getModelMutableLiveData().setValue(mBookDetails);
//                        getModelMutableLiveData().setValue(books);
                    }

                    @Override
                    public void onError(Throwable e) {
                        loadData();
                    }
                }));
    }

    public MutableLiveData<BookDetails> getModelMutableLiveData() {
        if(mBookDetails==null) loadData();
        return modelMutableLiveData;
    }
    @Override
    protected void onCleared() {
        super.onCleared();
        if (disposable != null) {
            disposable.clear();

        }
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getSortKey() {
        return sortKey;
    }

    public void setSortKey(String sortKey) {
        this.sortKey = sortKey;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @BindingAdapter({ "loadUrl" })
    public static void loadUrl(WebView webView, String description) {
        String html = HTML_HEADER + "<body>" + (description) + "</body>";
        webView.loadDataWithBaseURL(ASSETS_PREFIX, html, "text/html", "UTF-8", null);
        webView.setBackgroundColor(Color.TRANSPARENT);
    }
}
