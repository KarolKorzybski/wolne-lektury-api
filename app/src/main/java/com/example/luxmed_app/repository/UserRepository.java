package com.example.luxmed_app.repository;

import com.example.luxmed_app.model.Book;
import com.example.luxmed_app.model.BookDetails;
import com.example.luxmed_app.rest.BookService;

import java.util.ArrayList;

import javax.inject.Inject;

import io.reactivex.Single;

public class UserRepository {

    private BookService bookService;

    @Inject
    public UserRepository(BookService bookService) {
        this.bookService = bookService;
    }

    public Single<ArrayList<Book>> modelSingle() {
        return bookService.getBooks();
    }
    public Single<BookDetails> modelDetailsSingle(String url) {
        return bookService.getDetailsBooks(url);
    }
}
