package com.example.luxmed_app.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.example.luxmed_app.R;
import com.example.luxmed_app.callback.CallbackClick;
import com.example.luxmed_app.databinding.AdapterRow;
import com.example.luxmed_app.model.Book;
import com.example.luxmed_app.viewmodel.BooksViewModel;

import java.util.List;

public class BookAdapter extends RecyclerView.Adapter<BookAdapter.BookView> implements CallbackClick {
    private List<Book> arrayList;
    private LayoutInflater layoutInflater;
    private BooksViewModel viewModel;
    public BookAdapter(List<Book> channels, BooksViewModel viewModel) {
        this.arrayList = channels;
        this.viewModel = viewModel;
    }

    @Override
    public BookView onCreateViewHolder(final ViewGroup viewGroup, int i) {


        if(layoutInflater == null)
        {
            layoutInflater = LayoutInflater.from(viewGroup.getContext());
        }
        AdapterRow adapterRow = DataBindingUtil.inflate(layoutInflater, R.layout.row_book,viewGroup,false);
        adapterRow.setCallback(this);
        return new BookView(adapterRow);
    }

    @Override
    public void onBindViewHolder(BookView viewHolder, int position) {

        Book book = arrayList.get(position);
        viewHolder.bind(book);
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    @Override
    public void click(Book book) {
        if(viewModel.isClick()) return;
        viewModel.getComplete().set(false);
        viewModel.setClick(true);
        book.setClick(true);
        book.setBusy(false);
        viewModel.setModelMutableUrlDetails(book);
    }


    public class BookView extends RecyclerView.ViewHolder {

        private AdapterRow adapterRow;
        public BookView(AdapterRow adapterRow) {
            super(adapterRow.getRoot());
            this.adapterRow = adapterRow;
        }
        public void bind(Book book)
        {
            this.adapterRow.setBook(book);
            adapterRow.executePendingBindings();
        }

        public AdapterRow getAdapterRow()
        {
            return adapterRow;
        }

    }
}
