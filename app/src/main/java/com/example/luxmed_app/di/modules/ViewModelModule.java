package com.example.luxmed_app.di.modules;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.example.luxmed_app.di.ViewModelKey;
import com.example.luxmed_app.viewmodel.BookDetailsViewModel;
import com.example.luxmed_app.viewmodel.BooksViewModel;
import com.example.luxmed_app.viewmodel.ViewModelFactory;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

@Module
public abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(BooksViewModel.class)
    abstract ViewModel bindViewModel(BooksViewModel viewModel);
//
    @Binds
    @IntoMap
    @ViewModelKey(BookDetailsViewModel.class)
    abstract ViewModel bindViewModelDetails(BookDetailsViewModel viewModel);

    @Binds
    abstract ViewModelProvider.Factory bindFactory(ViewModelFactory factory);

}
