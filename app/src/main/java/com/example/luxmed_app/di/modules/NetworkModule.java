package com.example.luxmed_app.di.modules;

import com.example.luxmed_app.rest.BookService;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module(includes = ViewModelModule.class)
public abstract class NetworkModule {
        private final static String url = "https://wolnelektury.pl";
        @Provides
        @Singleton
        static Retrofit provideRetrofit() {
            return new Retrofit.Builder()
                    .baseUrl(url)
                    .client(okHttpClient())
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .build();
        }

        @Provides
        @Singleton
        static BookService provideUserService(Retrofit retrofit) {
            return retrofit.create(BookService.class);
        }

        private static OkHttpClient okHttpClient(){
                HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
                OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
                interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
                return client;
        }
}
