package com.example.luxmed_app.di.components;

import com.example.luxmed_app.di.modules.ContextModule;
import com.example.luxmed_app.di.modules.NetworkModule;
import com.example.luxmed_app.view.BookDetailsActivity;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {NetworkModule.class, ContextModule.class})
public interface AppComponentDetails {

    void inject(BookDetailsActivity activity);
}
