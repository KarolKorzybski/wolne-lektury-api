package com.example.luxmed_app.callback;

import com.example.luxmed_app.model.Book;

public interface CallbackClick {
    void click(Book book);
}
